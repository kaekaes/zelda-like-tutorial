﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour {

	private Transform target;
	private float ArIzX, ArIzY, AbDeX, AbDeY;

	void Awake () { 
		target = GameObject.FindGameObjectWithTag("Player").transform;
	}

	void Update () {
		transform.position = new Vector3(
			Mathf.Clamp(target.position.x,ArIzX,AbDeX),
			Mathf.Clamp(target.position.y,AbDeY,ArIzY),
			transform.position.z);
	}

	public void Limites (GameObject map){
		Tiled2Unity.TiledMap config = map.GetComponent<Tiled2Unity.TiledMap>();
		float cameraSize = Camera.main.orthographicSize;

		ArIzX = map.transform.position.x + cameraSize;
		ArIzY = map.transform.position.y - cameraSize;
		AbDeX = map.transform.position.x + config.NumTilesWide - cameraSize;
		AbDeY = map.transform.position.y - config.NumTilesHigh + cameraSize;
	}

}
